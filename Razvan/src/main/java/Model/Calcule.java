package Model;

//import Polinom;

import java.util.HashMap;

public class Calcule {
    private HashMap<Integer,Float> rezultat;
    private Polinom polinomrez;


    public Calcule(){
        this.rezultat=new HashMap<Integer, Float>();
        this.polinomrez=new Polinom();
    }
    public void adunaScade(Polinom polinom1,Polinom polinom2,int caz){
        this.polinomrez.clearMonoame();
        this.polinomrez.getPolinoame().values().clear();
        this.polinomrez.getPolinoame().keySet().clear();
        this.polinomrez.getPolinomSimplificat().clear();
        this.rezultat.clear();
        for(Integer puteri:polinom1.getPolinomSimplificat().keySet()){
            if(polinom2.getPolinomSimplificat().containsKey(puteri)){
                if(caz==0){
                    float suma=polinom1.getPolinomSimplificat().get(puteri)+polinom2.getPolinomSimplificat().get(puteri);
                    if(suma!=0){
                        this.rezultat.put(puteri,suma);
                    }
                }else{
                    float dif=polinom1.getPolinomSimplificat().get(puteri)-polinom2.getPolinomSimplificat().get(puteri);
                    if(dif!=0){
                        this.rezultat.put(puteri,dif);
                    }
                }
            }else{
                this.rezultat.put(puteri,polinom1.getPolinomSimplificat().get(puteri));
            }
        }
        for(Integer puteri:polinom2.getPolinomSimplificat().keySet()){
            if(!polinom1.getPolinomSimplificat().containsKey(puteri)){
                if(caz==0){
                    this.rezultat.put(puteri,polinom2.getPolinomSimplificat().get(puteri));
                }else{
                    this.rezultat.put(puteri,-polinom2.getPolinomSimplificat().get(puteri));
                }

            }
        }
        this.polinomrez.setPolinomSimplificat(this.rezultat);
    }

    public void inmultire(Polinom polinom1, Polinom polinom2){
        this.polinomrez.clearMonoame();
        this.polinomrez.getPolinoame().values().clear();
        this.polinomrez.getPolinoame().keySet().clear();
        this.polinomrez.getPolinomSimplificat().clear();
        for(Integer puteri:polinom1.getPolinomSimplificat().keySet()){ ;
            for(Integer puteri1:polinom2.getPolinomSimplificat().keySet()){
                this.polinomrez.add(puteri+puteri1,polinom1.getPolinomSimplificat().get(puteri)*polinom2.getPolinomSimplificat().get(puteri1));
            }
        }
        this.polinomrez.simplifica();
    }


    public String imparte(Polinom polinom1,Polinom polinom2){
        this.polinomrez.clearMonoame();
        this.polinomrez.getPolinoame().values().clear();
        this.polinomrez.getPolinoame().keySet().clear();
        this.polinomrez.getPolinomSimplificat().clear();
        Polinom polrest=new Polinom();
        Polinom polrez=new Polinom();
        Polinom polcat=new Polinom();
        polrest.simplifica();
        polrest.setPolinomSimplificat(polinom1.copiePolSimp(polinom1.getPolinomSimplificat()));
        if(polinom1.getPutereMaxima()<polinom2.getPutereMaxima()){
            return "Polinomul 2 are grad mai mare decat polinomul 1";
        }
        if(polinom1.getPutereMaxima()==0 && polinom2.getPutereMaxima()==0){
            return imparteCaz2(polinom1,polinom2);
        }
        if(polinom2.getPutereMaxima()==0){
            return imparteCaz1(polinom1,polinom2);
        }
        do{
            int putereMaxima1=polrest.getPutereMaxima();
            float paramPutereMaxima1=polrest.getParamPutereMaxima();
            int putereMaxima2=polinom2.getPutereMaxima();
            float paramPutereMaxima2=polinom2.getParamPutereMaxima();
            int putereCat=putereMaxima1-putereMaxima2;
            float paramCat=paramPutereMaxima1/paramPutereMaxima2;
            Polinom aux=new Polinom();
            aux.add(putereCat,paramCat);
            aux.simplifica();
            polcat.add(putereCat,paramCat);
            polcat.simplifica();
            inmultire(polinom2,aux);
            polrez.setPolinomSimplificat(this.polinomrez.copiePolSimp(this.polinomrez.getPolinomSimplificat()));
            adunaScade(polrest,polrez,1);
            polrest.setPolinomSimplificat(this.polinomrez.copiePolSimp(this.polinomrez.getPolinomSimplificat()));
        }while (polrest.getPutereMaxima()>=polinom2.getPutereMaxima());
        return "Catul: "+polcat.toString()+" Restul: "+polrest.toString();
    }

    public String imparteCaz1(Polinom polinom1,Polinom polinom2){
        float nr=polinom2.getParamPutereMaxima();
        this.polinomrez.clearMonoame();
        this.polinomrez.getPolinoame().values().clear();
        this.polinomrez.getPolinoame().keySet().clear();
        this.polinomrez.getPolinomSimplificat().clear();
        for(Integer puteri:polinom1.getPolinomSimplificat().keySet()){ ;
            this.polinomrez.add(puteri,polinom1.getPolinomSimplificat().get(puteri)/nr);
        }
        return this.polinomrez.toString();
    }

    public String imparteCaz2(Polinom polinom1,Polinom polinom2){
        this.polinomrez.clearMonoame();
        this.polinomrez.getPolinoame().values().clear();
        this.polinomrez.getPolinoame().keySet().clear();
        this.polinomrez.getPolinomSimplificat().clear();
       return polinom1.getParamPutereMaxima()/polinom2.getParamPutereMaxima()+"";
    }

    public void deriveaza(Polinom polinom){
        this.polinomrez.clearMonoame();
        this.polinomrez.getPolinoame().values().clear();
        this.polinomrez.getPolinoame().keySet().clear();
        this.polinomrez.getPolinomSimplificat().clear();
        int putere;
        float param;
        for(Integer puteri:polinom.getPolinomSimplificat().keySet()){
            putere=puteri-1;
            param=polinom.getPolinomSimplificat().get(puteri)*puteri;
            if(putere>=0){
                this.polinomrez.add(putere,param);
            }
        }
        this.polinomrez.simplifica();
    }

    public void integreaza(Polinom polinom1){
        this.polinomrez.clearMonoame();
        this.polinomrez.getPolinoame().values().clear();
        this.polinomrez.getPolinoame().keySet().clear();
        this.polinomrez.getPolinomSimplificat().clear();
        for(Integer puteri:polinom1.getPolinomSimplificat().keySet()){
            int putere1=puteri+1;
            this.polinomrez.add(putere1,polinom1.getPolinomSimplificat().get(puteri)/(float)putere1);
        }
    }

    public Polinom getPolinomrez() {
        return polinomrez;
    }

}
