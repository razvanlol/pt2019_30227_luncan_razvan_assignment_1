package Model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ProcesarePolinom {

    private Polinom polinom1;
    private Polinom polinom2;
    private int nrpol=0;

    public ProcesarePolinom(){
        this.polinom1=new Polinom();
        this.polinom2=new Polinom();
    }

    public boolean corectScris(String exp){
        boolean ok=true;
        for(int i = 0; i < exp.length(); ++i) {
            if (exp.charAt(i) != 'x' && exp.charAt(i) != '^' && exp.charAt(i) != '+' && exp.charAt(i) != '-' && exp.charAt(i) != '0' && exp.charAt(i) != '1' && exp.charAt(i) != '2' && exp.charAt(i) != '3' && exp.charAt(i) != '4' && exp.charAt(i) != '5' && exp.charAt(i) != '9' && exp.charAt(i) != '8' && exp.charAt(i) != '7' && exp.charAt(i) != '6') {
                ok = false;
            }
        }
        if(exp.contains("-^") || exp.contains("+^") || exp.contains("^-") || exp.contains("^+") || exp.contains("^x")){
            ok=false;
        }
        return ok;
    }

    public boolean corectScrisPuteri(String string){
        boolean ok=true;
        int nr=0;
        int nr1=0;
        for(int i = 0; i< string.length(); i++) {
            if (string.charAt(i) == '^') { ++nr; }

            if (string.charAt(i) == 'x') { ++nr1; }
        }
        if (nr > 1) { ok = false; }
        if (nr1 > 1) { ok = false; }
        return ok;
    }

    public boolean verificarePuteriSiCoeficienti(String exp){
        for(int i=1; i<exp.length()-1; i++){
            if(exp.charAt(i)=='^' && Character.isDigit(exp.charAt(i-1)) && Character.isDigit(exp.charAt(i+1))){
                return false;
            }
        }
        for(int i=0; i<exp.length()-1; i++){
            if(exp.charAt(i)=='x' && Character.isDigit(exp.charAt(i+1))){
                return false;
            }
        }
        return true;
    }

    public boolean verif(String test){
        String exp = test;
        Pattern pattern = Pattern.compile("([+-]?[^-+]+)");
        Matcher matcher = pattern.matcher(exp);
        boolean ok1=verificarePuteriSiCoeficienti(exp) && corectScris(exp);
        String sir="";
        int putere=0;
        int param=0;
        int caz=0;
        if(ok1){
            while (matcher.find()) {
                ok1=corectScrisPuteri(matcher.group(1));
                if(ok1==false){ return ok1; }
                sir = sir + matcher.group(1);
                if(!matcher.group(1).contains("^") && matcher.group(1).contains("x")){ caz=0; }
                if (!matcher.group(1).contains("x") && !matcher.group(1).contains("^")){ caz=1; }
                if (matcher.group(1).contains("x") && matcher.group(1).contains("^")){ caz=2; }
                if(matcher.group(1).contains("^") && !matcher.group(1).contains("x")){ok1=false;};
                switch (caz) {
                    case 0:
                        putere = 1;
                        if (matcher.group(1).contains("-")) {
                            if(matcher.group(1).equals("-x")){
                                param =-1;
                            }else{
                                String delims = "[-x]+";
                                String[] tokens = matcher.group(1).split(delims);
                                param = Integer.parseInt(tokens[1]);
                                param = -param;
                            }
                        } else {
                            if(matcher.group(1).contains("+")){
                                if(matcher.group(1).equals("+x")){
                                    param =1;
                                }else{
                                    String delims = "[+x]+";
                                    String[] tokens = matcher.group(1).split(delims);
                                    param = Integer.parseInt(tokens[1]);
                                }
                            }else{
                                if(matcher.group(1).equals("x")){
                                    param=1;
                                }else{
                                    String delims = "[x]+";
                                    String[] tokens = matcher.group(1).split(delims);
                                    param = Integer.parseInt(tokens[0]);
                                }

                            }
                        }
                        break;
                    case 1:
                        putere = 0;
                        if (matcher.group(1).contains("-")) {
                            String delims = "[-x]+";
                            String []tokens = matcher.group(1).split(delims);
                            param = Integer.parseInt(tokens[1]);
                            param = -param;
                        } else {
                            if(matcher.group(1).contains("+")){
                                String delims = "[+x]+";
                                String[] tokens = matcher.group(1).split(delims);
                                param = Integer.parseInt(tokens[1]);
                            }else{
                                String delims = "[+x]+";
                                String[] tokens = matcher.group(1).split(delims);
                                param = Integer.parseInt(tokens[0]);
                            }
                        }
                        break;
                    case 2:
                        if (matcher.group(1).contains("-")) {
                            String delims = "[-^x]+";
                            String[] tokens = matcher.group(1).split(delims);
                            if(matcher.group(1).contains("-x^")){
                                param=-1;
                                putere=Integer.parseInt(tokens[1]);
                            }else{
                                param = Integer.parseInt(tokens[1]);
                                param = -param;
                                putere = Integer.parseInt(tokens[2]);
                            }
                        } else {
                            if(matcher.group(1).contains("+")){
                                String delims = "[+^x]+";
                                String[] tokens = matcher.group(1).split(delims);
                                if(matcher.group(1).contains("+x^")){
                                    param=1;
                                    putere=Integer.parseInt(tokens[1]);
                                }else{
                                    param = Integer.parseInt(tokens[1]);
                                    putere = Integer.parseInt(tokens[2]);
                                }
                            }else{
                                String delims = "[+^x]+";
                                if (matcher.group(1).charAt(0) == 'x') {
                                    String[] tokens = matcher.group(1).split(delims);
                                    param=1;
                                    putere = Integer.parseInt(tokens[1]);
                                }else{
                                    String[] tokens = matcher.group(1).split(delims);
                                    param = Integer.parseInt(tokens[0]);
                                    putere = Integer.parseInt(tokens[1]);
                                }

                            }
                        }
                }
                if(ok1){
                    Monom monom=new Monom(putere,param);
                    if(nrpol%2==0){
                        this.polinom1.addMonom(monom);
                    }else{
                        this.polinom2.addMonom(monom);
                    }
                }
            }

        }
        nrpol++;
        if(exp.length()!=sir.length()){ ok1=false;  }
        return ok1;
    }

    public Polinom getPolinom1() {
        return polinom1;
    }

    public Polinom getPolinom2() {
        return polinom2;
    }
}
