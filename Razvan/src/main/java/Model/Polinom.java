package Model;

import Model.Monom;

import java.util.ArrayList;
import java.util.HashMap;

public class Polinom{
    private HashMap<Integer,ArrayList<Float>> polinoame;
    private HashMap<Integer,Float> polinomSimplificat;

    public Polinom(){
        this.polinoame=new HashMap<Integer, ArrayList<Float>>();
        this.polinomSimplificat=new HashMap<Integer, Float>();
    }

    public void addMonom(Monom monom){
        if(!this.polinoame.containsKey(monom.getPutere())){
            this.polinoame.put(monom.getPutere(),new ArrayList<Float>());
        }
        this.polinoame.get(monom.getPutere()).add(monom.getParametru());
    }

    public void add(int key,float val){
        if(!this.polinoame.containsKey(key)){
            this.polinoame.put(key,new ArrayList<Float>());
        }
        this.polinoame.get(key).add(val);
    }

    public HashMap<Integer, Float> getPolinomSimplificat() {
        return polinomSimplificat;
    }

    public HashMap<Integer,Float> copiePolSimp(HashMap<Integer,Float> original){
        HashMap<Integer,Float> copie=new HashMap<Integer, Float>();
        for(Integer intreg: original.keySet()){
            copie.put(intreg,original.get(intreg));
        }
        return copie;
    }

    public int getPutereMaxima(){
        ArrayList<Integer> puteri1=new ArrayList<Integer>(getPolinomSimplificat().keySet());
        if(puteri1.size()!=0){
            int putere1=puteri1.get(puteri1.size()-1);
            return putere1;
        }
       return 0;
    }

    public void inverseaza(){
        for(Integer intreg: this.polinomSimplificat.keySet()){
            float param=this.polinomSimplificat.get(intreg);
            float rez=-param;
            this.polinomSimplificat.put(intreg,rez);
        }
    }

    public float getParamPutereMaxima(){
        ArrayList<Integer> puteri1=new ArrayList<Integer>(getPolinomSimplificat().keySet());
        float param1=0;
        if(puteri1.size()!=0){
            param1=getPolinomSimplificat().get(puteri1.get(puteri1.size()-1));
        }
        return param1;
    }

    public void  simplifica() {
        for (Integer putere : this.polinoame.keySet()) {
            float suma = 0;
            for (Float parametru : this.polinoame.get(putere)) {
                suma += parametru;
            }
            this.polinomSimplificat.put(putere, suma);
            HashMap<Integer,Float> aux=new HashMap<Integer, Float>();
            aux=copiePolSimp(this.polinomSimplificat);
            for (Integer putereidx : aux.keySet()) {
                if (aux.get(putereidx) == 0) {
                    this.polinomSimplificat.remove(putere);
                }
            }
        }
    }
    public String toString(){
        simplifica();
        String rezultat="";
        for(Integer putere:this.polinomSimplificat.keySet()){
            if(putere==0){
                rezultat+=this.polinomSimplificat.get(putere);
            }else{
                if(putere==1){
                    if(this.polinomSimplificat.get(putere)>0){
                        if(this.polinomSimplificat.get(putere)==1){
                            rezultat+="+x";
                        }else{
                            rezultat+="+"+this.polinomSimplificat.get(putere)+"x";
                        }
                    }else{
                        if(this.polinomSimplificat.get(putere)==-1){
                            rezultat+="-x";
                        }else{
                            rezultat+=this.polinomSimplificat.get(putere)+"x";
                        }
                    }
                }else{
                    if(this.polinomSimplificat.get(putere)>0){
                        if(this.polinomSimplificat.get(putere)==1){
                            rezultat+="+x^"+putere;
                        }else{
                            rezultat+="+"+this.polinomSimplificat.get(putere)+"x^"+putere;
                        }
                    }else{
                        if(this.polinomSimplificat.get(putere)==-1){
                            rezultat+="-x^"+putere;
                        }else{
                            rezultat+=this.polinomSimplificat.get(putere)+"x^"+putere;
                        }
                    }
                }
            }
        }
        return rezultat;
    }

    public void setPolinomSimplificat(HashMap<Integer, Float> polinomSimplificat) {
        this.polinomSimplificat = polinomSimplificat;
    }

    public void setPolinoame(HashMap<Integer, ArrayList<Float>> polinoame) {
        this.polinoame = polinoame;
    }

    public HashMap<Integer, ArrayList<Float>> getPolinoame() {
        return polinoame;
    }

    public void clearMonoame(){this.polinoame.clear();}

}
