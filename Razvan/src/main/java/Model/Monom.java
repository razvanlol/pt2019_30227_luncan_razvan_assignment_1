package Model;

public class Monom {
    private int putere;
    private float parametru;

    public Monom(int putere,int parametru){
        this.putere=putere;
        this.parametru=parametru;
    }

    public float getParametru() {
        return parametru;
    }

    public int getPutere() {
        return putere;
    }

    public String toString(){
        return this.parametru+" "+this.putere+"   ";
    }
}
