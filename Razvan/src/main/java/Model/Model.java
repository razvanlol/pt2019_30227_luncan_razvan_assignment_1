package Model;

//import Polinom;

public class Model {

    private Calcule calcule;
    private ProcesarePolinom procesarePolinom;

    public Model(){
        this.calcule=new Calcule();
        this.procesarePolinom=new ProcesarePolinom();
    }

    public boolean verif(String test){
        return this.procesarePolinom.verif(test);
    }

    public void adunaScade(Polinom polinom1, Polinom polinom2, int caz){
        this.calcule.adunaScade(polinom1,polinom2,caz);
    }

    public void inmultire(Polinom polinom1, Polinom polinom2){
        this.calcule.inmultire(polinom1,polinom2);
    }

    public String imparte(Polinom polinom1,Polinom polinom2){ return this.calcule.imparte(polinom1,polinom2);}

    public void deriveaza(Polinom polinom){
        this.calcule.deriveaza(polinom);
    }

    public void integreaza(Polinom polinom){this.calcule.integreaza(polinom);}

    public Polinom getPolinomrez(){
        return this.calcule.getPolinomrez();
    }

    public Polinom getPolinom1(){
        return this.procesarePolinom.getPolinom1();
    }

    public Polinom getPolinom2(){
        return this.procesarePolinom.getPolinom2();
    }
}
