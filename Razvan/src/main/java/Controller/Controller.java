package Controller;

import Model.*;
//import Polinom;
import View.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {

    private View view;
    private Model model;

    public Controller(View view, Model model) {
        this.view = view;
        this.model = model;
        this.view.butonAduna(new butonAduna());
        this.view.butonScade(new butonScade());
        this.view.butonMul(new butonMul());
        this.view.butonDiv(new butonImp());
        this.view.butonDer(new butonDer());
        this.view.butonInt(new butonInt());
    }

    public class butonAduna implements ActionListener {
        //@Override
        public void actionPerformed(ActionEvent e) {
            model.getPolinom1().clearMonoame();
            model.getPolinom2().clearMonoame();
            String pol1 = view.getPolinom1().getText();
            String pol2 = view.getPolinom2().getText();
            if (model.verif(pol1) && model.verif(pol2)) {
                view.getPolinom1simpl().setText(model.getPolinom1().toString());
                view.getPolinom2simpl().setText(model.getPolinom2().toString());
                model.adunaScade(model.getPolinom1(), model.getPolinom2(), 0);
                view.getPolrez().setText(model.getPolinomrez().toString());
            } else {
                view.getPolrez().setText("Polinom introdus gresit!");
            }
        }
    }

    public class butonScade implements ActionListener {
       // @Override
        public void actionPerformed(ActionEvent e) {
            model.getPolinom1().clearMonoame();
            model.getPolinom2().clearMonoame();
            String pol1 = view.getPolinom1().getText();
            String pol2 = view.getPolinom2().getText();
            if (model.verif(pol1) && model.verif(pol2)) {
                view.getPolinom1simpl().setText(model.getPolinom1().toString());
                view.getPolinom2simpl().setText(model.getPolinom2().toString());
                model.adunaScade(model.getPolinom1(), model.getPolinom2(), 1);
                view.getPolrez().setText(model.getPolinomrez().toString());
            } else {
                view.getPolrez().setText("Polinom introdus gresit!");
            }
        }
    }

    public class butonMul implements ActionListener {
        //@Override
        public void actionPerformed(ActionEvent e) {
            model.getPolinom1().clearMonoame();
            model.getPolinom2().clearMonoame();
            String pol1 = view.getPolinom1().getText();
            String pol2 = view.getPolinom2().getText();
            if (model.verif(pol1) && model.verif(pol2)) {
                view.getPolinom1simpl().setText(model.getPolinom1().toString());
                view.getPolinom2simpl().setText(model.getPolinom2().toString());
                model.inmultire(model.getPolinom1(), model.getPolinom2());
                view.getPolrez().setText(model.getPolinomrez().toString());
            } else {
                view.getPolrez().setText("Polinom introdus gresit!");
            }
        }
    }

    public class butonImp implements ActionListener {
        //@Override
        public void actionPerformed(ActionEvent e) {
            model.getPolinom1().clearMonoame();
            model.getPolinom2().clearMonoame();
            String pol1 = view.getPolinom1().getText();
            String pol2 = view.getPolinom2().getText();
            if (model.verif(pol1) && model.verif(pol2)) {
                view.getPolinom1simpl().setText(model.getPolinom1().toString());
                view.getPolinom2simpl().setText(model.getPolinom2().toString());
                model.imparte(model.getPolinom1(), model.getPolinom2());
                view.getPolrez().setText(model.imparte(model.getPolinom1(), model.getPolinom2()));
            } else {
                view.getPolrez().setText("Polinom introdus gresit!");
            }
        }
    }

    public class butonDer implements ActionListener {
       // @Override
        public void actionPerformed(ActionEvent e) {
            model.getPolinom1().clearMonoame();
            model.getPolinom2().clearMonoame();
            String pol1 = view.getPolinom1().getText();
            if (model.verif(pol1)) {
                view.getPolinom1simpl().setText(model.getPolinom1().toString());
                model.deriveaza(model.getPolinom1());
                view.getPolrez().setText(model.getPolinomrez().toString());
            } else {
                view.getPolrez().setText("Polinom introdus gresit!");
            }
        }
    }

    public class butonInt implements ActionListener {
       // @Override
        public void actionPerformed(ActionEvent e) {
            model.getPolinom1().clearMonoame();
            model.getPolinom2().clearMonoame();
            String pol1 = view.getPolinom1().getText();
            if (model.verif(pol1)) {
                view.getPolinom1simpl().setText(model.getPolinom1().toString());
                model.integreaza(model.getPolinom1());
                view.getPolrez().setText(model.getPolinomrez().toString());
            } else {
                view.getPolrez().setText("Polinom introdus gresit!");
            }
        }
    }
}


