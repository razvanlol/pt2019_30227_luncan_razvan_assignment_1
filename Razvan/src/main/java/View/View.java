package View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class View extends JFrame {

    private JLabel p1;
    private JLabel p1s;
    private JLabel p2s;
    private JLabel p2;
    private JLabel rezultat;
    private JTextField polinom1;
    private JTextField polinom1simpl;
    private JTextField polinom2;
    private JTextField polinom2simpl;
    private JTextField polrez;

    private JButton aduna;
    private JButton scade;
    private JButton inmulteste;
    private JButton imparte;
    private JButton deriveaza;
    private JButton integreaza;

    private JPanel polinoame;
    private JPanel butoane;

    public View(){
        this.polinoame=new JPanel();
        this.butoane=new JPanel();
        this.aduna=new JButton("Aduna");
        this.scade=new JButton("Scade");
        this.inmulteste=new JButton("Inmulteste");
        this.imparte=new JButton("Imparte");
        this.deriveaza=new JButton("Deriveaza");
        this.integreaza=new JButton("Integreaza");
        this.p1=new JLabel("Polinomul 1");
        this.polinom1=new JTextField("");
        this.polinom1simpl=new JTextField("        ");
        this.polinom1simpl.setEnabled(false);
        this.polinom1simpl.setForeground(Color.RED);
        this.polinom2=new JTextField("");
        this.polinom2simpl=new JTextField("       ");
        this.polinom2simpl.setEnabled(false);
        this.p1s=new JLabel("Polinomul 1 simplificat");
        this.p2=new JLabel("Polinomul 2");
        this.p2s=new JLabel("Polinimul 2 simplificat:");
        this.rezultat=new JLabel("Rezultat");
        this.polrez=new JTextField("      ");
        this.polrez.setEnabled(false);
        this.polinoame.setLayout(new BoxLayout(polinoame,BoxLayout.Y_AXIS));
        this.polinoame.add(p1);
        this.polinoame.add(polinom1);
        this.polinoame.add(p1s);
        this.polinoame.add(polinom1simpl);
        this.polinoame.add(p2);
        this.polinoame.add(polinom2);
        this.polinoame.add(p2s);
        this.polinoame.add(polinom2simpl);
        this.polinoame.add(rezultat);
        this.polinoame.add(polrez);
        this.butoane.setLayout(new BoxLayout(butoane,BoxLayout.X_AXIS));
        this.butoane.add(aduna);
        this.butoane.add(scade);
        this.butoane.add(inmulteste);
        this.butoane.add(imparte);
        this.butoane.add(deriveaza);
        this.butoane.add(integreaza);
        this.setTitle("Calculator Polinoame");
        this.setPreferredSize(new Dimension(900,500));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        polinoame.add(butoane);
        this.add(polinoame);
        this.pack();
    }

    public JTextField getPolinom1(){
        return polinom1;
    }

    public JTextField getPolinom2(){return  polinom2;}

    public JTextField getPolinom1simpl(){return  polinom1simpl;}

    public JTextField getPolinom2simpl(){return  polinom2simpl;}

    public JTextField getPolrez(){return polrez;}

    public void butonAduna(ActionListener a1){
        aduna.addActionListener(a1);
    }

    public void butonScade(ActionListener a2){
        scade.addActionListener(a2);
    }

    public void butonDiv(ActionListener a3){
        imparte.addActionListener(a3);
    }

    public void butonMul(ActionListener a4){
        inmulteste.addActionListener(a4);
    }

    public void butonDer(ActionListener a5){
        deriveaza.addActionListener(a5);
    }

    public void butonInt(ActionListener a6){
        integreaza.addActionListener(a6);
    }

}