import Model.Calcule;
import Model.ProcesarePolinom;
import org.junit.Test;
import static org.junit.Assert.*;

public class TestJU {
    @Test
    public void testAdunare(){
        ProcesarePolinom procesarePolinom=new ProcesarePolinom();
        Calcule calcule=new Calcule();
        procesarePolinom.verif("x^2+4x^1+x^0");
        procesarePolinom.getPolinom1().simplifica();
        procesarePolinom.verif("3x-2x+1");
        procesarePolinom.getPolinom2().simplifica();
        calcule.adunaScade(procesarePolinom.getPolinom1(),procesarePolinom.getPolinom2(),0);
        assertEquals("2.0+5.0x+x^2",""+calcule.getPolinomrez().toString());

        ProcesarePolinom procesarePolinom1=new ProcesarePolinom();
        Calcule calcule1=new Calcule();
        procesarePolinom1.verif("x^2");
        procesarePolinom1.getPolinom1().simplifica();
        procesarePolinom1.verif("-x^2");
        procesarePolinom1.getPolinom2().simplifica();
        calcule.adunaScade(procesarePolinom1.getPolinom1(),procesarePolinom1.getPolinom2(),0);
        assertNotEquals("3x",""+calcule1.getPolinomrez().toString());

    }

    @Test
    public void testScadere(){
        ProcesarePolinom procesarePolinom=new ProcesarePolinom();
        Calcule calcule=new Calcule();
        procesarePolinom.verif("-x^2-x+1");
        procesarePolinom.getPolinom1().simplifica();
        procesarePolinom.verif("x");
        procesarePolinom.getPolinom2().simplifica();
        calcule.adunaScade(procesarePolinom.getPolinom1(),procesarePolinom.getPolinom2(),1);
        assertEquals("1.0-2.0x-x^2",""+calcule.getPolinomrez().toString());

        ProcesarePolinom procesarePolinom1=new ProcesarePolinom();
        Calcule calcule1=new Calcule();
        procesarePolinom1.verif("-x^2+x^3+3x^3+3x^1+1");
        procesarePolinom1.getPolinom1().simplifica();
        procesarePolinom1.verif("-x^2+9x+12x^0");
        procesarePolinom1.getPolinom2().simplifica();
        calcule1.adunaScade(procesarePolinom1.getPolinom1(),procesarePolinom1.getPolinom2(),1);
        assertNotEquals("-11.0-6.0x^2+4.0x^3",""+calcule1.getPolinomrez().toString());

    }

    //3x^2+4x+1

    @Test
    public void testInmultire(){
        ProcesarePolinom procesarePolinom=new ProcesarePolinom();
        Calcule calcule=new Calcule();
        procesarePolinom.verif("3x^2+4x+1");
        procesarePolinom.getPolinom1().simplifica();
        procesarePolinom.verif("4x^3-2x^3+5x^6+12x+3x^0");
        procesarePolinom.getPolinom2().simplifica();
        calcule.inmultire(procesarePolinom.getPolinom1(),procesarePolinom.getPolinom2());
        assertEquals("3.0+24.0x+57.0x^2+38.0x^3+8.0x^4+6.0x^5+5.0x^6+20.0x^7+15.0x^8",""+calcule.getPolinomrez().toString());

        ProcesarePolinom procesarePolinom1=new ProcesarePolinom();
        Calcule calcule1=new Calcule();
        procesarePolinom1.verif("-1");
        procesarePolinom1.getPolinom1().simplifica();
        procesarePolinom1.verif("3x+1");
        procesarePolinom1.getPolinom2().simplifica();
        calcule1.inmultire(procesarePolinom1.getPolinom1(),procesarePolinom1.getPolinom2());
        assertNotEquals("-1.0x-3.0x",""+calcule1.getPolinomrez().toString());

    }

    @Test
    public void testImpartire(){
        ProcesarePolinom procesarePolinom=new ProcesarePolinom();
        Calcule calcule=new Calcule();
        procesarePolinom.verif("x^3-2x^2+3x-1");
        procesarePolinom.getPolinom1().simplifica();
        procesarePolinom.verif("x+2");
        procesarePolinom.getPolinom2().simplifica();
        calcule.imparte(procesarePolinom.getPolinom1(),procesarePolinom.getPolinom2());
        assertEquals("-23.0",""+calcule.getPolinomrez().toString());

        ProcesarePolinom procesarePolinom1=new ProcesarePolinom();
        Calcule calcule1=new Calcule();
        procesarePolinom1.verif("6x^3+4x^2+2x+1");
        procesarePolinom1.getPolinom1().simplifica();
        procesarePolinom1.verif("3");
        procesarePolinom1.getPolinom2().simplifica();
        calcule1.imparte(procesarePolinom1.getPolinom1(),procesarePolinom1.getPolinom2());
        assertNotEquals("0.33333334x+0.6666667x+1.3333334x^2+2.0x^3",""+calcule1.getPolinomrez().toString());

    }

    @Test
    public void testDerivare(){
        ProcesarePolinom procesarePolinom=new ProcesarePolinom();
        Calcule calcule=new Calcule();
        procesarePolinom.verif("3x+5x^2-3x^0+2x");
        procesarePolinom.getPolinom1().simplifica();
        calcule.deriveaza(procesarePolinom.getPolinom1());
        assertEquals("5.0+10.0x",""+calcule.getPolinomrez().toString());

        ProcesarePolinom procesarePolinom1=new ProcesarePolinom();
        Calcule calcule1=new Calcule();
        procesarePolinom1.verif("3x-x^2-9");
        procesarePolinom1.getPolinom1().simplifica();
        calcule1.deriveaza(procesarePolinom1.getPolinom1());
        assertNotEquals("3.0x-2.0x",""+calcule1.getPolinomrez().toString());

    }

    @Test
    public void testIntegrare(){
        ProcesarePolinom procesarePolinom=new ProcesarePolinom();
        Calcule calcule=new Calcule();
        procesarePolinom.verif("-x^2+3x^2-x^2+6x+x^0");
        procesarePolinom.getPolinom1().simplifica();
        calcule.integreaza(procesarePolinom.getPolinom1());
        assertEquals("+x+3.0x^2+0.33333334x^3",""+calcule.getPolinomrez().toString());

        ProcesarePolinom procesarePolinom1=new ProcesarePolinom();
        Calcule calcule1=new Calcule();
        procesarePolinom1.verif("1+2+3");
        procesarePolinom1.getPolinom1().simplifica();
        calcule1.integreaza(procesarePolinom1.getPolinom1());
        assertNotEquals("+6.0x^2",""+calcule1.getPolinomrez().toString());

    }
}
